module.exports = {
    fields: {
        id: {
            type: "uuid",
            default: { "$db_function": "uuid()" }
        },
        first_name: "text",
        last_name: "text",
        search: {
            type: "set",
            typeDef: "<text>"
        }
    },
    key: ["id"],
    indexes: ["search"]
}
