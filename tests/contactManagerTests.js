var should = require('should');
var sinon = require('sinon');

var contactManager = require('../contactManager')();

describe("Contact Manager", function() {

  it("should return a list of contacts", function() {
    var req = {},
      res = {
        send: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.get(req, res);

    sinon.assert.calledWith(res.send, [
      { id: '1', first_name: 'John', last_name: 'Doe' },
      { id: '2', first_name: 'Bill', last_name: 'Johnson' }
    ]);
  });

  it("should return contacts by id", function() {
    var
      req = {
        params: {
          id: '2'
        }
      },
      res = {
        send: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.getById(req, res);

    sinon.assert.calledWith(res.send,
      { id: '2', first_name: 'Bill', last_name: 'Johnson' }
    );
    sinon.assert.calledWith(res.status, 200);
  });

  it("should return 404 when contact not found", function() {
    var
      req = {
        params: {
          id: '3'
        }
      },
      res = {
        send: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.getById(req, res);

    sinon.assert.calledWith(res.status, 404);
  });

  it("should create a new contact", function() {
    var
      req = {
        body: { first_name: 'New', last_name: 'Contact' }
      },
      res = {
        json: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.post(req, res);

    sinon.assert.calledWith(res.status, 201);
  });

  it("should return 400 when new contact missing", function() {
    var
      req = {
        body: {}
      },
      res = {
        send: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.post(req, res);

    sinon.assert.calledWith(res.status, 400);
  });

  it("should update a contact", function() {
    var
      req = {
        params: { id: '4' },
        body: { id: '4', first_name: 'Existing', last_name: 'Contact' }
      },
      res = {
        json: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.put(req, res);

    sinon.assert.calledWith(res.status, 200);
  });

  it("should return 400 when updated contact missing", function() {
    var
      req = {
        params: { id: '4' },
        body: { }
      },
      res = {
        send: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.put(req, res);

    sinon.assert.calledWith(res.status, 400);
  });

  it("should remove a contact", function() {
    var
      req = {
        params: { id: '2' }
      },
      res = {
        send: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.delete(req, res);

    sinon.assert.calledWith(res.status, 200);
  });

  it("should return 404 when contact to be removed not found", function() {
    var
      req = {
        params: { id: '4' }
      },
      res = {
        send: sinon.spy(),
        status: sinon.spy()
      };

    contactManager.delete(req, res);

    sinon.assert.calledWith(res.status, 404);
  });
});
