var express = require('express'),
    router = require('./router')(express),
    bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api/contacts', router);

var port = process.env.PORT || 3000;

app.use(express.static('./webroot'));

app.listen(port, function () {
    console.log('Contacts running on port ' + port);
});
