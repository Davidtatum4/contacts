var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    mocha = require('gulp-mocha'),
    concat = require('gulp-concat'),
    webpack = require('gulp-webpack'),
    path = require("path");

gulp.task('test', function () {
    return gulp.src('tests/*.js', { read: false })
        .pipe(mocha({ reporter: 'nyan' }));
});

gulp.task('default', function () {
    nodemon({
            script: 'app.js',
            ext: 'js jsx',
            ignore: [
                'node_modules/',
                'webroot/',
                'webapp/build/'
            ],
            tasks: function (changedFiles) {
                var tasks = [];
                changedFiles.forEach(function (file) {
                    if (file.indexOf('/webapp/') > 0 && !~tasks.indexOf('webpack')) {
                        tasks.push('webpack');
                    }
                });
                return tasks;
            }
        })
        .on('restart', function () {
            console.log('restarting...');
        });

});

gulp.task('webpack', function () {
    return gulp.src(["./webapp/*.js", "./webapp/*.jsx"])
        .pipe(webpack(require("./webpack.config.js")))
        .pipe(concat('webapp_bundle.js'))
        .pipe(gulp.dest('webroot'));
});
