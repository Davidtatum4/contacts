var models = require('./models')

var contactManager = function () {

    function mapContactFromRequest(req, contact) {
        contact.first_name = req.body.first_name;
        contact.last_name = req.body.last_name;
        contact.search = getSearchValues([contact.first_name, contact.last_name]);
        return contact;
    }

    function getSearchValues(terms) {
        var search = [];
        terms.forEach(function (term) {
            var value = '';
            term.split('').forEach(function (each) {
                value += each.toLowerCase();
                search.push(value);
            });
        });
        return search;
    }

    function sendError(res, message) {
        console.log(message);
        res.status(400);
        res.send(message);
    }

    function sendNotFound(res) {
        res.status(404);
        res.send('Not found');
    }

    function sendSuccess(res, result) {
        res.status(200);
        res.send(result);
    }

    return {
        get: function (req, res) {
            if (req.query.q) {
                var query = 'SELECT * FROM "contacts"."contact" WHERE search CONTAINS ?';
                var params = [req.query.q.toLowerCase()];
                models.instance.Contact.execute_query(query, params, function (err, result) {
                    if (err) {
                        sendError(res, err);
                    } else {
                        sendSuccess(res, result.rows);
                    }
                });
            } else {
                models.instance.Contact.find({}, function (err, contacts) {
                    if (err) {
                        sendError(res, err);
                    } else {
                        sendSuccess(res, contacts);
                    }
                });
            }
        },
        getById: function (req, res) {
            models.instance.Contact.findOne({ id: req.params.id }, function (err, contact) {
                if (err) {
                    sendError(res, err);
                } else if (contact) {
                    sendSuccess(res, contact);
                } else {
                    sendNotFound(res);
                }
            });

        },
        post: function (req, res) {
            if (req.body.first_name && req.body.last_name) {
                var newContact = mapContactFromRequest(req, new models.instance.Contact());

                newContact.save(function (err) {
                    if (err) {
                        sendError(res, err);
                    } else {
                        sendSuccess(res, newContact);
                    }
                });

            } else {
                sendError(res, 'Invalid');
            }
        },
        put: function (req, res) {
            if (req.body.first_name && req.body.last_name) {
                models.instance.Contact.findOne({ id: req.params.id }, function (err, contact) {
                    if (err) {
                        rsendError(res, 'Invalid');
                    } else {
                        if (contact) {
                            contact = mapContactFromRequest(req, contact);

                            contact.save(function (err) {
                                if (err) {
                                    sendError(res, err);
                                } else {
                                    sendSuccess(res, contact);
                                }
                            });
                        } else {
                            sendNotFound(res);
                        }
                    }
                });
            } else {
                sendError(res, 'Invalid');
            }
        },
        delete: function (req, res) {
            models.instance.Contact.findOne({ id: req.params.id }, function (err, result) {
                if (err) {
                    sendError(res, err);
                } else {
                    if (result) {
                        result.delete(function (err) {
                            if (err) {
                                rsendError(res, err);
                            } else {
                                sendSuccess(res, 'Removed');
                            }
                        });
                    } else {
                        sendNotFound(res);
                    }
                }
            });
        }
    };
};

module.exports = contactManager;
