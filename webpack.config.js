var webpack = require('webpack');

module.exports = {
    entry: "./webapp/Main.jsx",
    output: { path: __dirname, filename: './webapp/build/main.js' },
    module: {
        loaders: [{
            test: /.jsx?$/,
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
                presets: ['es2015', 'react']
            }
        }]
    },
};
