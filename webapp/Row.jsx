var React = require('react');

var Row = React.createClass({
    propTypes: {
            contact: React.PropTypes.object.isRequired
    },
    render: function () {
        var contact = this.props.contact;

        var initials = ((contact.first_name ? contact.first_name.charAt(0) : "") +
            (contact.last_name ? contact.last_name.charAt(0) : "")).toUpperCase();

        return (
            <div>
                <div className="row">
                  <div className="col-md-2 col-sm-3 hidden-xs text-center">
                      <div className="avatar-circle">
                        <span className="initials">{ initials }</span>
                      </div>
                  </div>
                  <div className="col-md-10 col-sm-9">
                    <h3>{ contact.first_name } { contact.last_name }</h3>
                    <div className="row">
                        <div className="col-xs-9">
                            <h4>{ contact.email }</h4>
                            <h4>{ contact.phone }</h4>
                        </div>
                    </div>
                  </div>
                </div>
                <hr></hr>
            </div>
            )
        }
    });

module.exports = Row;
