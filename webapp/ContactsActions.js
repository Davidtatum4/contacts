var Dispatcher = require('./ConactsDispatcher');
var Constants = require('./ContactsConstants');

var ContactsActions = {

    create: function (contact) {
        Dispatcher.dispatch({
            actionType: Constants.CREATE,
            contact: contact
        });
    },

    update: function (contact) {
        Dispatcher.dispatch({
            actionType: Constants.UPDATE,
            contact: contact
        });
    },

    remove: function (id) {
        AppDispatcher.dispatch({
            actionType: Constants.REMOVE,
            id: id
        });
    }

};

module.exports = ContactsActions;
