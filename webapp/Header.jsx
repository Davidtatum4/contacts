var React = require('react');

var Header = React.createClass({
    render: function () {
        return (
        <div id="masthead">
		  <div className="container">
			<div className="row">
			  <div className="col-md-7">
				<h2>Contacts
				  <p className="lead"></p>
				</h2>
			  </div>
			  <div className="col-md-5">
				<div className="well well-lg">
				  <div className="row">
					<div className="col-sm-12">
						<form className="form-horizontal">
							<div className="input-group">
							   <input type="text" className="form-control" placeholder="Search" />
							   <span className="input-group-btn">
		                           <button className="btn btn-default" type="submit"><i className="glyphicon glyphicon-search" /></button>
							   </span>
							</div>
						</form>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>

		  <div className="container">
			<div className="row">
			  <div className="col-md-12">
				<div className="top-spacer"> </div>
			  </div>
			</div>
		  </div>

		</div>

            )
        }
    });

module.exports = Header;
