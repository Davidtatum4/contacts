var React = require('react');
var ReactDom = require('react-dom');
var Header = require('./Header.jsx');
var Row = require('./Row.jsx');
var Store = require('./ContactsStore');

function getContactsState() {
    return {
        contacts: Store.getAll()
    };
}

var Main = React.createClass({
    getInitialState: function() {
      return getContactsState();
    },

    componentDidMount: function() {
      Store.addChangeListener(this._onChange);
    },

    componentWillUnmount: function() {
      Store.removeChangeListener(this._onChange);
    },

    _onChange: function() {
      this.setState(getContactsState());
    },

    render: function () {

        var list = this.state.contacts.map(contact => {
            return (
                <Row key={contact.id} id={contact.id} contact={ contact }></Row>
            );
        });
        return (
            <div>
                <Header ></Header>
                <div className="container">
        		  <div className="row">
        		    <div className="col-md-12">
        		      <div className="panel">
        		        <div className="panel-body">
                            { list }
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            )
        }
    });

ReactDom.render(<Main/> , document.getElementById('webapp'));
