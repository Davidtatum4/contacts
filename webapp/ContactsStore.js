var Dispatcher = require('./ContactsDispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('./ContactsConstants');

function create(contact) {

}

function update(contact) {

}

function remove(id) {

}

const CHANGE_EVENT = 'change';

class ContactsStore extends EventEmitter {

    constructor() {
        super();
        this.contacts = [
            { id: 1, first_name: "Sally", last_name: "Johnson", email: "sallyj@gmail.com", phone: "(800) 654-3210" },
            { id: 2, first_name: "Bill", last_name: "Johnson", email: "billyj@aol.com", phone: "(987) 123-4567" },
            { id: 3, first_name: "William", last_name: "Jones", email: "billyjones@yahoo.com", phone: "(804) 555-1212" },
            { id: 4, first_name: "Herman", last_name: "Brown", email: "brown_h@gmail.com", phone: "(800) 555-3210" },
            { id: 5, first_name: "Trey", last_name: "McMillan", email: "wmc@aol.com", phone: "(987) 234-4567" },
            { id: 6, first_name: "Jill", last_name: "Lund", email: "jlund@yahoo.com", phone: "(804) 555-1234" }
        ];
    }


    getAll() {
        return this.contacts;
    }

    addChangeListener(listener, context) {
        this.on(CHANGE_EVENT, listener, context);
    }

    removeChangeListener(listener, context) {
        this.removeListener(CHANGE_EVENT, listener, context);
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

}

const contactsStore = new ContactsStore();

contactsStore.dispatchToken = Dispatcher.register(action => {
    switch (action.type) {

    case ActionTypes.CREATE:
        contactsStore.contacts.push(action.contact);
        contactsStore.emitChange();
        break;

    case ActionTypes.UPDATE:
        //const index = contactsStore.contacts.findIndex(x => x.id === action.id);
        //if (index !== 1) {
        //    contactsStore.contacts.slice(index, 1);
        //    contactsStore.emitChange();
        //}
        break;

    case ActionTypes.REMOVE:
        const index = contactsStore.contacts.findIndex(x => x.id === action.id);
        if (index !== 1) {
            contactsStore.contacts.slice(index, 1);
            contactsStore.emitChange();
        }
        break;

    default:
        // Do nothing
    }
});

module.exports = contactsStore;
