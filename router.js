module.exports = function (express) {
    var router = express.Router();
    var contactManager = require('./contactManager')();

    router.get('/', contactManager.get);
    router.get('/:id', contactManager.getById);
    router.post('/', contactManager.post);
    router.put('/:id', contactManager.put);
    router.delete('/:id', contactManager.delete);

    return router;
};
